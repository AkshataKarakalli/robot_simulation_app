class Api::RobotsController < ApplicationController
    def orders
        input = params[:commands]
        a = execute(input)
        render json: a, status: :ok
    end
    private
    def execute(input)
          a=[]
          x = 0
          y = 0
    current_direction = 'NORTH'
    input.each do |c|
    if c[0..4] == 'PLACE'
    cords = c.split(' ')[1].split(',')
    x = cords[0].to_i
    y = cords[1].to_i
    current_direction = cords[2]
    
    elsif c == 'MOVE'
    if current_direction == 'NORTH'
    y += 1 if y >= 0 && y < 6
    elsif current_direction == 'SOUTH'
    y -= 1 if y >= 0 && y < 6
    elsif current_direction == 'EAST'
    x += 1 if x >= 0 && x < 6
    elsif current_direction == 'WEST'
    x -= 1 if x >= 0 && x < 6
    end
    elsif c == 'LEFT'
    # W <- N <- E <- S <- W
    current_direction = if current_direction == 'NORTH'
    'WEST'
    elsif current_direction == 'SOUTH'
    'EAST'
    elsif current_direction == 'EAST'
    'NORTH'
    else
    'SOUTH'
    end
    elsif c == 'RIGHT'
    # N -> E -> S -> W -> N
    current_direction = if current_direction == 'NORTH'
    'EAST'
    elsif current_direction == 'SOUTH'
    'WEST'
    elsif current_direction == 'EAST'
    'SOUTH'
    else
    'NORTH'
    end
    elsif c == 'REPORT'
    # puts "#{x}, #{y}, #{current_direction}"
    a.push(x,y,current_direction) 
    end
       end
     a 
      end
    
end
